import { NavLink, Outlet } from "react-router-dom";
import { useState } from "react";

import hamburgerLogo from "../image/hamburger.svg";
import closeLogo from "../image/close.svg";
import reactLogo from "../image/react.gif";
import homeLogo from "../image/home.png";

export default function Sidebar() {
  const [isOpen, setIsOpen] = useState(true);

  return (
    <div>
      {isOpen ? (
        <div className="sidebar">
          <img
            src={hamburgerLogo}
            alt="Hamburger Logo"
            className="logo"
            onClick={() => setIsOpen(false)}
          />

          <h1 className="logo">REACT</h1>
          <ul>
            <li>
              <NavLink to="/" className="active">
                Home
              </NavLink>
            </li>

            <li>
              <NavLink to="/UseState" className="active">
                UseState
              </NavLink>
            </li>
            <li>
              <NavLink to="/UseEffect" className="active">
                UseEffect
              </NavLink>
            </li>
            <li>
              <NavLink to="/UseReducer" className="active">
                UseReducer
              </NavLink>
            </li>
            <li>
              <NavLink to="/about" className="active">
                About
              </NavLink>
            </li>
          </ul>

          <main>
            <Outlet />
          </main>
        </div>
      ) : (
        <div className="sidebar small">
          <img
            src={closeLogo}
            alt="Close Logo"
            className="displaylogo"
            onClick={() => setIsOpen(true)}
          />

          <h1>
            <img
              src={reactLogo}
              alt="React Logo"
              className="displaylogo-react"
            />
          </h1>
          <ul>
            <li>
              <NavLink to="/" className="active">
                <img src={homeLogo} alt="Home Logo" className="displaylogo" />
              </NavLink>
            </li>

            <li>
              <NavLink to="/UseState" className="active">
                <h2 className="displaylogo" style={{ color: "blue" }}>
                  S
                </h2>
              </NavLink>
            </li>
            <li>
              <NavLink to="/UseEffect" className="active">
                <h2 className="displaylogo" style={{ color: "red" }}>
                  E
                </h2>
              </NavLink>
            </li>
            <li>
              <NavLink to="/about" className="active">
                <h2 className="displaylogo" style={{ color: "purple" }}>
                  ℹ
                </h2>
              </NavLink>
            </li>
          </ul>

          <Outlet></Outlet>
        </div>
      )}
    </div>
  );
}
