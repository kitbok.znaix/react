// create a home function
export default function About() {
  return (
    <div className="content">
      <div className="about">
        <h2>ABOUT</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita
          natus modi quas dolor? Perferendis nulla ipsum ipsam amet perspiciatis
          voluptatibus, ratione, fugiat error architecto odit necessitatibus,
          aut eveniet omnis praesentium? Lorem ipsum dolor sit amet consectetur
          adipisicing elit. Id quisquam hic labore adipisci voluptas ullam
          repudiandae iure explicabo molestias et, corporis veritatis, culpa
          similique voluptates, neque recusandae? Sint, amet quos. Repellendus
          aliquam dolor, perspiciatis unde iure adipisci ex ab qui quod error
          laboriosam mollitia. Quis, deserunt corrupti. Ipsa voluptates,
          reiciendis molestiae, velit illum at a quibusdam voluptatum dolorem
          facilis nisi. Culpa delectus saepe asperiores sed ullam, reiciendis
          sit eaque! Veniam iusto recusandae quaerat, suscipit esse cupiditate
          delectus numquam vitae voluptatem doloribus harum omnis neque nihil
          soluta tempora quis minus reprehenderit? Vitae, vel aliquam, ipsa quae
          velit veritatis officia nesciunt doloremque, assumenda deserunt quo.
          Ab fugiat adipisci, mollitia unde, officia esse eius qui a accusantium
          facere cumque dolorum eligendi nostrum ducimus? Voluptatum
          perspiciatis provident soluta voluptates reprehenderit? Eum explicabo,
          facilis ea optio dolore sunt temporibus quidem suscipit. Harum minima
          enim voluptatibus ex reiciendis aliquid odit iure tempora, animi quam
          necessitatibus exercitationem! Ratione dolorem, excepturi atque illum
          nobis blanditiis itaque culpa perspiciatis consequuntur assumenda
          repellendus tenetur quos asperiores qui totam. Similique nisi aliquam
          accusantium, corrupti labore libero eveniet nulla animi molestiae rem.
        </p>
      </div>
    </div>
  );
}
