import StepMover from "../component/StepMover";

const UseState = () => {
  return (
    <div>
      <StepMover person="Kitbok" key="1" />
      <StepMover person="Bolmy" key="2" />
      <StepMover person="Hame" key="3" />
      <StepMover person="Paul" key="4" />
    </div>
  );
};

export default UseState;
