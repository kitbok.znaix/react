import React, { useEffect, useState } from "react";

const carData = [
  {
    brand: "Toyota",
    model: "Camry",
    year: 2022,
    price: 25000,
    available: true,
  },
  {
    brand: "Honda",
    model: "Accord",
    year: 2022,
    price: 26000,
    available: true,
  },
  {
    brand: "Ford",
    model: "Mustang",
    year: 2023,
    price: 40000,
    available: true,
  },
  {
    brand: "Chevrolet",
    model: "Camaro",
    year: 2023,
    price: 38000,
    available: false,
  },
  {
    brand: "BMW",
    model: "X5",
    year: 2022,
    price: 55000,
    available: true,
  },
];

export default function App() {
  const [car, setCar] = useState({});

  function getCar() {
    const randomIndex = Math.floor(Math.random() * carData.length);
    const selectedCar = carData[randomIndex];
    setCar(selectedCar);
  }

  function resetView() {
    setCar({});
  }

  useEffect(() => {
    getCar();
  }, []);

  return (
    <div className="content">
      <h1>
        {car.brand} {car.model}
      </h1>
      <p>Year: {car.year}</p>
      <p>Price: {car.price}</p>
      <button onClick={getCar}>Get Car</button>
      <button onClick={resetView}>Reset View</button>
    </div>
  );
}
