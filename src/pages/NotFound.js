import { Link } from "react-router-dom";

export default function NotFound() {
  return (
    <div className="content">
      <h1>Page Not Found ☹️</h1>
      <p>
        This is probably because you have not taken the requested path, or the
        path does not exist on this server.
      </p>
      <p>
        Go to the <Link to="/">Homepage</Link>
      </p>
    </div>
  );
}
