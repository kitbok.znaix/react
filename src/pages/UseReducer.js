import { useReducer } from "react";

function reducer(state, action) {
  switch (action.type) {
    case "INCREMENT":
      return {
        count: state.count + 1,
        color: state.color,
      };
    case "toggleColor":
      return {
        count: state.count,
        color: randomizeColor(),
      };
    case "reset":
      return {
        count: 0,
        color: "red",
      };
    default:
      return state;
  }
}

function randomizeColor() {
  const letters = "0123456789ABCDEF";
  let color = "#";

  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }

  return color;
}

export default function RandomizeColor() {
  const [state, dispatch] = useReducer(reducer, {
    count: 0,
    color: "red", // Set the default color to "red"
  });

  function handleIncrement() {
    dispatch({ type: "INCREMENT" });
    dispatch({ type: "toggleColor" });
  }

  function handleReset() {
    dispatch({ type: "reset" });
  }

  return (
    <div className="content">
      <h1 style={{ color: state.color }}>{state.count}</h1>
      <button onClick={handleIncrement}>Generate Random Color</button>
      <button onClick={handleReset}>Reset</button>
      <p>Color: {state.color}</p>
    </div>
  );
}
