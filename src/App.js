import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

// Import components
import Sidebar from "./layout/Sidebar";
import Home from "./pages/Home";
import About from "./pages/About";
import UseState from "./pages/UseState";
import UseEffect from "./pages/UseEffect";
import UseReducer from "./pages/UseReducer";
import NotFound from "./pages/NotFound";

export default function App() {
  return (
    <Router>
      <div className="app">
        <Sidebar />
        <main>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/UseState" element={<UseState />} />
            <Route path="/UseEffect" element={<UseEffect />} />
            <Route path="/UseReducer" element={<UseReducer />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </main>
      </div>
    </Router>
  );
}
